#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) 2023 Francesco Cusolito, mous16@gmail.com
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
"""
Description of this extension
"""

import inkex
import math
import os

class ExportSelectionAligned(inkex.EffectExtension):

    @staticmethod
    def approx(value, up):
        truncated = math.trunc(value)
        if math.isclose(value, truncated, abs_tol=1e-09):
            return truncated

        if up:
            return math.ceil(value)

        return math.floor(value)

    def add_arguments(self, pars):
        pars.add_argument("--dir", action="store", dest="dir", default="~/", help="")

    def effect(self):
        if not inkex.command.is_inkscape_available():
            inkex.errormsg('Error: inkscape executable not available')
            return


        input_file = os.path.expanduser(self.options.input_file)
        output_dir = os.path.expanduser(self.options.dir)

        string = ""
        for item in self.svg.selection:
            id = item.eid

            name = item.label
            if not name:
                name = id
            name = str(name)

            box = item.bounding_box(item.composed_transform())
            left = ExportSelectionAligned.approx(item.unit_to_viewport(box.left), False)
            top = ExportSelectionAligned.approx(item.unit_to_viewport(box.top), False)
            right = ExportSelectionAligned.approx(item.unit_to_viewport(box.right), True)
            bottom= ExportSelectionAligned.approx(item.unit_to_viewport(box.bottom), True)

            output_file = os.path.join(output_dir, "_".join([name, str(left), str(top)])+".png")
            #self.msg("{} cmd: ".format(name)+" ".join([input_file,
            #                                           '--export-type="png"',
            #                                           '--export-filename="{}"'.format(output_file),
            #                                           '--export-id="{}"'.format(id),
            #                                           '--export-id-only',
            #                                           '--export-area="{}:{}:{}:{}"'.format(left, top, right, bottom)]))

            try:
                output = inkex.command.inkscape(input_file,
                                                export_type='png',
                                                export_filename='{}'.format(output_file),
                                                export_id='{}'.format(id),
                                                export_id_only=True,
                                                export_area='{}:{}:{}:{}'.format(left, top, right, bottom))
                #self.msg('Command output: '+ str(output))

            except inkex.InkscapeCommandFailed as e:
                inkex.errormsg('Inkscape command failed: ' + str(e))
                return

            except inkex.command.ProgramRunError as e:
                inkex.errormsg('Inkscape run error: ' + str(e))
                return



if __name__ == '__main__':
    ExportSelectionAligned().run()
